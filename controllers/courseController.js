module.exports.addCourse = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if(userData.isAdmin == false){
            return "You are not an admin"
        }else{
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
            // Saves the created object to the database
            return newCourse.save().then((course, error) => {
                // Course creation failed
                if(error){
                    return false
                }else{
                    // Course creation successful
                    return "Course creation successful"
                }
            })
        }
    })
}

module.exports.getAllCourses = () => {
    return Course.find( {} ).then(result => {
        return result
    })
}